variable "app_name" {
  default = "demo-app-prod"
}

variable "AMI" {
  default = "ami-13be557e"
}

variable "ec2_type" {
  default = "t2.medium"
}

variable "key_name" {
  default = "drew"
}
