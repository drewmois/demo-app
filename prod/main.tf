module "webserver" {
  source     = "../modules/webserver"

  ec2_type   = var.ec2_type
  AWS_REGION = var.AWS_REGION
  AMI        = var.AMI
  key_name   = var.key_name
  app_name   = var.app_name
}