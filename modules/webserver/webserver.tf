variable "AMI" {}
variable "ec2_type" {}
variable "AWS_REGION" {}
variable "key_name" {}
variable "app_name" {}



module "eg_prod_bastion_label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=master"

  tags = {
    "Name"        = var.app_name
    "BusinessUnit" = "XYZ",
    "Snapshot"     = "true"
  }
}

resource "aws_instance" "example" {
  ami           = var.AMI
  instance_type = var.ec2_type
  key_name      = var.key_name
  tags          = module.eg_prod_bastion_label.tags
}