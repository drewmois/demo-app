variable "app_name" {
  default = "demo-app-dev"
}

variable "AMI" {
  default = "ami-13be557e"
}

variable "ec2_type" {
  default = "t2.micro"
}

variable "key_name" {
  default = "drew"
}
