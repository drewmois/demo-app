terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "drewm-terraform-state"
    key            = "demo-app-dev/terraform.tfstate"
    region         = "us-east-1"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "drewm-terraform-state-locks"
    encrypt        = true
  }
}